package com.colomTours.login.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.colomTours.login.model.Login;

public class LoginRowMapper implements RowMapper<Login> {

	@Override
	public Login mapRow(ResultSet rs, int rowNum) throws SQLException {
		Login login = new Login();
		login.setUsu_id(rs.getLong("USU_ID"));
		login.setUsu_nombre(rs.getString("USU_NOMBRE"));
		login.setUsu_apellido(rs.getString("USU_APELLIDO"));
		login.setUsu_cedula(rs.getInt("USU_CEDULA"));
		login.setUsu_telefono(rs.getLong("USU_TELEFONO"));
		login.setUsu_direccion(rs.getString("USU_DIRECCION"));
		login.setUsu_email(rs.getString("USU_EMAIL"));
		login.setUsu_password(rs.getString("USU_PASSWORD"));
		login.setUsu_username(rs.getString("USU_USERNAME"));
		login.setRol_rol_id(rs.getInt("ROL_ROL_ID"));
		login.setTipo_documento_id(rs.getInt("TIPO_DOCUMENTO_ID"));
		return login;
	}

}
