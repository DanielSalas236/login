package com.colomTours.login.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.colomTours.login.dao.api.LoginJDBCDaoAPI;
import com.colomTours.login.model.Login;
import com.colomTours.login.rowmapper.LoginRowMapper;



@Repository
public class LoginJDBCDaoImpl implements LoginJDBCDaoAPI {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public LoginJDBCDaoImpl(JdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public List<Login> getAll() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM USUARIO");

		return jdbcTemplate.query(sql.toString(), new LoginRowMapper());
	}

	@Override
	public List<Login> buscarUsuario(String username, String password) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM USUARIO WHERE usu_username= '"+username+"' AND usu_password = '"+password+"'");
		return jdbcTemplate.query(sql.toString(), new LoginRowMapper());
	}
//	@Override
//	public List<Login> findById(String id, String password) {
//		StringBuilder sql = new StringBuilder();
//		sql.append("SELECT * FROM USUARIO");
//		return jdbcTemplate.query(sql.toString(), new LoginRowMapper());
//	}

}
