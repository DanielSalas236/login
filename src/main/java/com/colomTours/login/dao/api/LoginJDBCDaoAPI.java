package com.colomTours.login.dao.api;

import java.util.List;
import com.colomTours.login.model.Login;

public interface LoginJDBCDaoAPI {
	List<Login> getAll();
	List<Login> buscarUsuario(String usuario, String password);
}
