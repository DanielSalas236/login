package com.colomTours.login.authenticator;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Authenticator extends Remote{
	// Potential response strings for the authenticate method.
	//
	public final static String AUTHENTICATED = "AOK";
	public final static String AUTHENTICATION_FAILED = "ERR";
	public final static String AUTHENTICATION_CONTINUE = "MORE";
	// Accept a key string that should be the next response from
	// the client in the negotiation protocol, and return a prompt
	// string or status indication.
	//
	public String authenticate(String key)
	throws RemoteException;
	// Return the protected object. This method returns null until
	// the negotiation is successfully completed.
	//
	public Object get()
	throws RemoteException;
}
