package com.colomTours.login.authenticator;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class AuthenticatorImpl extends UnicastRemoteObject implements Authenticator{
	private ObjectFactory factory;
	private Object robject;
	public AuthenticatorImpl(ObjectFactory inFactory)
	throws RemoteException
	{
	super();
	factory = inFactory;
	robject = null;
	}
	public Object get()
	throws RemoteException
	{
	return robject;
	}
	public String authenticate(String key)
			throws RemoteException
			{
			String response = new String("");
			/* Application-specific authentication */
			if (response.equals(Authenticator.AUTHENTICATED))
			{
			robject = (Object) factory.create();
			}
			return response;
			}

}
