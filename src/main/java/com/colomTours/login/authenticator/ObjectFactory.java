package com.colomTours.login.authenticator;

public interface ObjectFactory {
public Object create();
}
