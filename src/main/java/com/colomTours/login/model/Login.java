package com.colomTours.login.model;

public class Login {
	private long usu_id;
	private String usu_nombre;
	private String usu_apellido;
	private int usu_cedula;
	private long usu_telefono;
	private String usu_direccion;
	private String usu_email;
	private String usu_password;
	private int rol_rol_id;
	private int tipo_documento_id;
	private String usu_username;
	
	
	public String getUsu_username() {
		return usu_username;
	}
	public void setUsu_username(String usu_username) {
		this.usu_username = usu_username;
	}
	private String mensaje;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public long getUsu_id() {
		return usu_id;
	}
	public void setUsu_id(long usu_id) {
		this.usu_id = usu_id;
	}
	public String getUsu_nombre() {
		return usu_nombre;
	}
	public void setUsu_nombre(String usu_nombre) {
		this.usu_nombre = usu_nombre;
	}
	public String getUsu_apellido() {
		return usu_apellido;
	}
	public void setUsu_apellido(String usu_apellido) {
		this.usu_apellido = usu_apellido;
	}
	public int getUsu_cedula() {
		return usu_cedula;
	}
	public void setUsu_cedula(int usu_cedula) {
		this.usu_cedula = usu_cedula;
	}
	public long getUsu_telefono() {
		return usu_telefono;
	}
	public void setUsu_telefono(long usu_telefono) {
		this.usu_telefono = usu_telefono;
	}
	public String getUsu_direccion() {
		return usu_direccion;
	}
	public void setUsu_direccion(String usu_direccion) {
		this.usu_direccion = usu_direccion;
	}
	public String getUsu_email() {
		return usu_email;
	}
	public void setUsu_email(String usu_email) {
		this.usu_email = usu_email;
	}
	public String getUsu_password() {
		return usu_password;
	}
	public void setUsu_password(String usu_password) {
		this.usu_password = usu_password;
	}
	public int getRol_rol_id() {
		return rol_rol_id;
	}
	public void setRol_rol_id(int rol_rol_id) {
		this.rol_rol_id = rol_rol_id;
	}
	public int getTipo_documento_id() {
		return tipo_documento_id;
	}
	public void setTipo_documento_id(int tipo_documento_id) {
		this.tipo_documento_id = tipo_documento_id;
	}
}
