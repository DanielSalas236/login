package com.colomTours.login.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.colomTours.login.dao.api.LoginJDBCDaoAPI;
import com.colomTours.login.model.Login;

@RestController
@RequestMapping(value = "/login")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class LoginController {
	

	@Autowired
	private LoginJDBCDaoAPI loginJDBCDaoAPI;
	
	@GetMapping(value = "/iniciar")
	public List<Login> buscarUsuario(@RequestParam String username, String password) {
		List<Login> result = loginJDBCDaoAPI.buscarUsuario(username, password);
		if(result.isEmpty() == true) {
			List<Login> error = new ArrayList<>();
			Login lg = new Login();
			lg.setUsu_id(1);
			lg.setMensaje("Usuario o contraseña inválidos");
			error.add(lg);
			return error;
		}else {
			return result;
		}
		
	}
}
